﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Management;
using System.Security.AccessControl;
using System.Security.Principal;

namespace _1
{
    class Program
    {
        public static bool HasWritePermissionOnDir(string path)
        {
            var writeAllow = false;
            var writeDeny = false;
            var accessControlList = Directory.GetAccessControl(path);
            if (accessControlList == null)
                return false;
            var accessRules = accessControlList.GetAccessRules(true, true, typeof(SecurityIdentifier));
            if (accessRules == null)
                return false;

            foreach (FileSystemAccessRule rule in accessRules)
            {
                if ((FileSystemRights.Write & rule.FileSystemRights) != FileSystemRights.Write)
                    continue;

                if (rule.AccessControlType == AccessControlType.Allow)
                    writeAllow = true;
                else if (rule.AccessControlType == AccessControlType.Deny)
                    writeDeny = true;
            }

            return writeAllow && !writeDeny;
        }

        static void Main(string[] args)
        {
            if (!HasWritePermissionOnDir(Environment.CurrentDirectory))
            {
                Console.WriteLine("ERROR: Нет права записи в текущую директорию!");
                return;
            }

            FileInfo fileInfo = new FileInfo(Assembly.GetExecutingAssembly().Location);

            File.Delete(fileInfo.FullName + ".old");

            byte[] fileBytes = File.ReadAllBytes(fileInfo.FullName);

            MD5 md5 = new MD5CryptoServiceProvider();

            int cnt = 50;
            byte[] fb = new byte[cnt];
            int j = 0;
            for (int i = fileBytes.Length - cnt; i < fileBytes.Length; i++)
            {
                fb[j] = fileBytes[i];
                j++;
            }

            string[] str = Encoding.UTF8.GetString(fb).Split(new char[] { '|' });

            if (str.Length > 1 && str[str.Length - 2] == "protect")
            {
                int fbl = fileBytes.Length - 33;
                byte[] checkSum = new byte[fbl];

                for (int i = 0; i < fbl; i++)
                {
                    checkSum[i] = fileBytes[i];
                }

                checkSum = md5.ComputeHash(checkSum);

                string cs = "";
                foreach (byte b in checkSum)
                {
                    cs += b.ToString("x2");
                }

                int n = Convert.ToInt32(str[str.Length - 3]);
                byte[] fbn = new byte[n];
                int j1 = 0;
                for (int i = fileBytes.Length - n; i < fileBytes.Length; i++)
                {
                    fbn[j1] = fileBytes[i];
                    j1++;
                }

                string driveletter = Environment.CurrentDirectory.Substring(0, 2);

                var index = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDiskToPartition").Get().Cast<ManagementObject>();
                var disks = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_DiskDrive").Get().Cast<ManagementObject>();
                var drive = (from i in index where i["Dependent"].ToString().Contains(driveletter) select i).FirstOrDefault();
                var key = drive["Antecedent"].ToString().Split('#')[1].Split(',')[0];

                var disk = (from dd in disks where dd["Name"].ToString() == "\\\\.\\PHYSICALDRIVE" + key select dd).FirstOrDefault();

                string deviceID = disk["Model"] + ";" + disk["SerialNumber"] + ";" + disk["PNPDeviceID"].ToString().Split('\\').Last();

                string[] strn = Encoding.UTF8.GetString(fbn).Split(new char[] { '|' });

                Console.WriteLine("Данные носителя:");
                Console.WriteLine(GetInfoDrive(deviceID));
                Console.WriteLine("Сохраненные данные носителя:");
                Console.WriteLine(GetInfoDrive(strn[strn.Length - 6]));
                Console.WriteLine("Совпадение:\t\t" + (deviceID == strn[strn.Length - 6]));
                Console.WriteLine();

                if (deviceID != strn[strn.Length - 6])
                {
                    Console.WriteLine("Не буду работать на этом носителе!!!");
                    return;
                }

                Console.WriteLine("Размер файла:\t\t\t\t" + fileInfo.Length);
                Console.WriteLine("Сохраненный размер файла:\t\t" + strn[strn.Length - 4]);
                Console.WriteLine("Совпадение:\t\t" + (fileInfo.Length.ToString() == strn[strn.Length - 4]));
                Console.WriteLine();
                Console.WriteLine("Имя файла:\t\t\t\t" + fileInfo.Name);
                Console.WriteLine("Сохраненное имя файла:\t\t\t" + strn[strn.Length - 7]);
                Console.WriteLine("Совпадение:\t\t" + (fileInfo.Name == strn[strn.Length - 7]));
                Console.WriteLine();
                string d = fileInfo.CreationTimeUtc.ToString().Substring(0, fileInfo.CreationTimeUtc.ToString().Length - 3);
                Console.WriteLine("Дата создания файла:\t\t\t" + d);
                Console.WriteLine("Сохраненная дата создания файла:\t" + strn[strn.Length - 5]);
                Console.WriteLine("Совпадение:\t\t" + (d == strn[strn.Length - 5]));
                Console.WriteLine();
                Console.WriteLine("Хэш файла:\t\t\t\t" + cs);
                Console.WriteLine("Сохраненный хэш файла:\t\t\t" + strn[strn.Length - 1]);
                Console.WriteLine("Совпадение:\t\t" + (cs == strn[strn.Length - 1]));
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Имя файла: " + fileInfo.Name);
                Console.WriteLine("Дата создания: " + fileInfo.CreationTimeUtc.ToString().Substring(0, fileInfo.CreationTimeUtc.ToString().Length - 3));
                Console.WriteLine("Размер в байтах: " + fileBytes.Length);

                string driveletter = Environment.CurrentDirectory.Substring(0, 2);

                var index = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDiskToPartition").Get().Cast<ManagementObject>();
                var disks = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_DiskDrive").Get().Cast<ManagementObject>();
                var drive = (from i in index where i["Dependent"].ToString().Contains(driveletter) select i).FirstOrDefault();
                var key = drive["Antecedent"].ToString().Split('#')[1].Split(',')[0];

                var disk = (from d in disks where d["Name"].ToString() == "\\\\.\\PHYSICALDRIVE" + key select d).FirstOrDefault();

                string deviceID = disk["Model"] + ";" + disk["SerialNumber"] + ";" + disk["PNPDeviceID"].ToString().Split('\\').Last();

                Console.WriteLine("\nModel: {0}\nSerialNumber: {1}\nPNPDeviceID: {2}\n", 
                    disk["Model"], 
                    disk["SerialNumber"], 
                    disk["PNPDeviceID"].ToString().Split('\\').Last());

                File.Move(fileInfo.FullName, fileInfo.FullName + ".old");

                string data = "|" + fileInfo.Name
                    + "|" + deviceID
                    + "|" + fileInfo.CreationTimeUtc.ToString().Substring(0, fileInfo.CreationTimeUtc.ToString().Length - 3)
                    + "|0000|000|protect";

                byte[] endata = Encoding.UTF8.GetBytes(data);

                data = "|" + fileInfo.Name
                    + "|" + deviceID
                    + "|" + fileInfo.CreationTimeUtc.ToString().Substring(0, fileInfo.CreationTimeUtc.ToString().Length - 3)
                    + "|" + (fileBytes.Length + endata.Length + 33)
                    + "|" + (endata.Length + 33)
                    + "|protect";
                endata = Encoding.UTF8.GetBytes(data);

                byte[] retBytes = new byte[fileBytes.Length + endata.Length];

                for (int i = 0; i < fileBytes.Length; i++)
                {
                    retBytes[i] = fileBytes[i];
                }

                int j1 = 0;
                for (int i = fileBytes.Length; i < fileBytes.Length + endata.Length; i++)
                {
                    retBytes[i] = endata[j1];
                    j1++;
                }

                byte[] checkSum = md5.ComputeHash(retBytes);

                string cs = "";
                foreach (byte b in checkSum)
                {
                    cs += b.ToString("x2");
                }

                Console.WriteLine("Хэш: " + cs);

                string s = "|" + cs;
                endata = Encoding.UTF8.GetBytes(s);

                BinaryWriter STG = new BinaryWriter(File.Open(fileInfo.FullName, FileMode.Create));
                foreach (byte b in retBytes)
                {
                    STG.Write(b);
                }

                foreach (byte b in endata)
                {
                    STG.Write(b);
                }

                STG.Close();

                Process.Start(fileInfo.FullName);
            }
        }

        static string GetInfoDrive(string str)
        {
            string[] s = str.Split(new char[] { ';' });
            if (s.Length < 3)
            {
                return "бугагага";
            }
            return string.Format("Model: {0}\nSerialNumber: {1}\nPNPDeviceID: {2}\n", s[0], s[1], s[2]);
        }
    }
}
